// const axios = require('axios');
// import axios from 'axios';

// ? berisi function yg di pakai utk index?

const tampilData = (data) => {
     let ul = document.getElementById("gameList");
     let content = "";
     data.forEach(d => {
          content += `<li> name: ${d.name} <br> genre: ${d.genre}</li>
                         <br />
                         <a class="btn btn-danger" onclick="deleteGame(${d.id})" name="delete">Delete</a> &nbsp;&nbsp;&nbsp;
                         <a onclick="detailGame(${d.id})" class="btn btn-success" name="update">Update</a> &nbsp;&nbsp;&nbsp;
                         <br /><br />`
          
     });
     return ul.innerHTML = content;
}


const getGames = () => {
     // * contoh pakai fetch
     // fetch("http://localhost:3000/")
     //      .then((response) => response.json())
     //      .then((resJSON) => {
     //           const data = resJSON.data;
     //           let ul = document.getElementById("gameList");
     //           let content = "";
     //           data.forEach(d => {
     //                content += `<li> id: ${d.id} <br> name ${d.name} <br> genre ${d.genre}</li> <br />
     //                               <a href="delete/${d.id}" class="btn btn-danger" name="delete">Delete</a> &nbsp;&nbsp;&nbsp;
     //                               <a href="update/${d.id}" class="btn btn-success" name="update">Update</a>`
                    
     //           });
     //           ul.innerHTML = content;
     //      })
     // .catch ((err) => { 
     //      console.log(err);
     // });
     
     axios.get("http://localhost:3000")
          .then((res) => {
               tampilData(res.data.data);
          })
          .catch((err) => {
               console.log("Error! : ", err);
          });
}

function addGames() {
     const nama = document.getElementById('nama').value;
     const genre = document.getElementById('genre').value;

     axios.post("http://localhost:3000/add", {
          nama, genre
     })
     .then((res) => {
          alert("berhasil menambahkan data");
          tampilData(res.data.data);
     })
     .catch((err) => {
          alert("Error: " + err.message);
     });   
}

const deleteGame = (gameId) => {
     axios.post(`http://localhost:3000/delete/${gameId}`)
          .then((res) => {
               alert("Berhasil menghapus data!");
               tampilData(res.data.data);
          })
          .catch((err) => {
               alert("Error : " + err.message);
          });
}

const updateGame = () => {
     const gameId = document.getElementById('editId').value;
     const name = document.getElementById('editNama').value;
     const genre = document.getElementById('editGenre').value;

     axios.post(`http://localhost:3000/update/${gameId}`, {
          name, genre
     })
     .then((res) => {
          alert("berhasil update data");
          tampilData(res.data.data);
     })
     .catch((err) => {
          alert("Error: " + err.message);
     });   
}

const detailGame = (gameId) => {
     console.log("function detail jalan = ", gameId);
     // window.location.href = "detail.html";
     axios.get(`http://localhost:3000/detail/${gameId}`)
          .then((res) => {
               const data = res.data.data;
               document.getElementById("editId").value = gameId;
               document.getElementById("editNama").value = data[0].name;
               document.getElementById("editGenre").value = data[0].genre;
          })
          .catch((err) => {
               alert("Error: " + err.message);    
          })
}

