const express = require("express");
const app = express();
const port = 3000;

const cors = require("cors");
const fs = require("fs");
const morgan = require("morgan"); 
const { request } = require("http");
app.use(morgan("dev"));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const PATH_DB = "./db/games.json";


// ? tempat atur data dan routing?

app.get("/", (req, res) => {
     fs.readFile(PATH_DB, (err, games) => {
          if (err) {
               res.status(400).json({
                    error: "Error reading file",
               });
               console.log(err);
          } else {
               res.status(200).json({
                    data: JSON.parse(games)
               });
          }
     });
});

app.post("/add", (req, res) => {
     // const nama = req.body.nama;
     // const genre = req.body.genre;

     const { nama, genre } = req.body;
     if (!nama || !genre) {
          res.status(400).json({
               error: "Mohon isi data yang kosong!",
          });
     } else {
          fs.readFile(PATH_DB, "utf8", (err, data) => {
               const getGames = JSON.parse(data);
               getGames.push({
                    id: getGames[getGames.length - 1].id + 1,
                    name: nama,
                    genre: genre,
               });
               fs.writeFile(PATH_DB, JSON.stringify(getGames), err => {
                    res.status(201).json({
                         message: "success!",
                         data: getGames,
                    })
               });
          });
     }
});

app.post('/delete/:id', (req, res) => {
     const gameID = JSON.parse(req.params.id);

     fs.readFile(PATH_DB, "utf8", (err, data) => {
          const getGames = JSON.parse(data);
          const gamesFiltered = getGames.filter(games => {
               return games.id !== gameID;
          });
          fs.writeFile(PATH_DB, JSON.stringify(gamesFiltered), err => {
               res.status(200).json({
                    message: "success delete game",
                    data: gamesFiltered
               })
          });
     });
});

app.post('/update/:id', (req, res) => {
     const gameID = req.params.id;
     const { name, genre } = req.body;

     fs.readFile(PATH_DB, "utf8", (err, data) => {
          const getGames = JSON.parse(data);
          getGames.forEach(games => {
               if (games.id == gameID) {
                    games.name = name;
                    games.genre = genre;
               }
          });
          fs.writeFile(PATH_DB, JSON.stringify(getGames), err => {
               res.status(200).json({
                    message: "berhasil ubah data!",
                    data: getGames
               });
          });
     });
});

app.get("/detail/:id", (req, res) => {
     const gameID = req.params.id;

     fs.readFile(PATH_DB, "utf8", (err, data) => {
          const getGames = JSON.parse(data);
          const gamesFiltered = getGames.filter(games => {
               return games.id == gameID;
          });
          // console.log("cek data = ",gamesFiltered);
          res.status(200).json({
               data: gamesFiltered
          });
     });
});

app.listen(port, () => { 
     console.log(`Example app listening on port ${port}`);
     console.log("Press Ctrl+C to quit.");
});  